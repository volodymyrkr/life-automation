const webpack = require('webpack');
const path = require('path');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

/*
 * We've enabled UglifyJSPlugin for you! This minifies your app
 * in order to load faster and run less javascript.
 *
 * https://github.com/webpack-contrib/uglifyjs-webpack-plugin
 *
 */
const HtmlWebpackPlugin = require("html-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const ENV_PRODUCTION = "production";
const ENV_DEVELOPMENT = "development";


const __ENV__ = process.env.__ENV__ || ENV_DEVELOPMENT;
const __USER__ = process.env.__USER__ || "unknown";

module.exports = {
	resolve: {
		modules: [path.resolve(__dirname, './src'), 'node_modules'],
		extensions: [".ts", ".tsx", ".js", ".json"]
	},

	module: {
		rules: [
			// All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
			{
				test: /\.tsx?$/,
				loader: "awesome-typescript-loader"
			},
			{
				include: [path.resolve(__dirname, 'src')],
				loader: 'babel-loader',

				options: {
					plugins: ['syntax-dynamic-import'],

					presets: [
						[
							'@babel/preset-env',
							{
								modules: false
							}
						]
					]
				},

				test: /\.js$/
			},
			{
				test: /\.(scss|css)$/,

				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader'
					}
				]
			}
		]
	},

	entry: {
		index: './src/bootstrap/index.ts',
	},

	output: {
		path: path.resolve(__dirname, 'dist')
	},

	devtool: "source-map",

	watch: false,

	watchOptions: {
		aggregateTimeout: 1000
	},

	mode: 'development',

	optimization: {
		splitChunks: {
			cacheGroups: {
				vendors: {
					priority: -10,
					test: /[\\/]node_modules[\\/]/
				}
			},

			chunks: 'async',
			minChunks: 1,
			minSize: 30000,
			name: true
		}
	},

	plugins: [
		new webpack.DefinePlugin({
			__ENV__: JSON.stringify(__ENV__),
			__USER__: JSON.stringify(__USER__)
		}),
		new HtmlWebpackPlugin({
			template: "./src/bootstrap/index.html"
		})
	]
};

if (__ENV__ === ENV_PRODUCTION) {
	module.exports.plugins.push(
		new UglifyJSPlugin({
			sourceMap: true,
		})
	)
}