export const log = (text) => {
    if (__ENV__ === "development") {
        console.log('%c%s', 'background: #2f9249; color: aliceblue; font: 1.2em Tahoma; display: block;', text);
    }
}