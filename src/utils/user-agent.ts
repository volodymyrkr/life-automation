import {UAParser} from "ua-parser-js";

export enum PlatformType {
    DESKTOP,
    MOBILE,
    TABLET
}

export function getPlatformType() {
    var ua = new UAParser();

    if (ua.getDevice().type === UAParser.DEVICE.MOBILE) {
        return PlatformType.MOBILE
    } else if (ua.getDevice().type === UAParser.DEVICE.TABLET) {
        return PlatformType.TABLET
    }
    return PlatformType.DESKTOP;
}
