import * as React from "react";
import DocumentHtmlComponent from "common/browser-state/document-html.component";
import SessionModalsComponent from "common/session-modals/session-modals.component";
import {IModuleConfig, withModule} from "common/with-module/with-module.component";
import {SessionStore} from "common/session-modals/session.store";

const config:IModuleConfig = {
  addBindings: (container) => {
    container.bind("SessionStore").to(SessionStore);
  }
};

@withModule(config)
class RootDesktopComponent extends React.Component {
    render() {
        return (
            <div>
                <DocumentHtmlComponent>
                  DESKTOP
                </DocumentHtmlComponent>
                <SessionModalsComponent/>
            </div>
        )
    }
}

export default RootDesktopComponent;