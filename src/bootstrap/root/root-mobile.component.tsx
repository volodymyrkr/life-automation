import * as React from "react";
import DocumentHtmlComponent from "common/browser-state/document-html.component";
import SessionModalsComponent from "common/session-modals/session-modals.component";

class RootMobileComponent extends React.Component {
    render() {
        return (
            <div>
                <DocumentHtmlComponent>
                    MOBILE
                </DocumentHtmlComponent>
                <SessionModalsComponent/>
            </div>
        )
    }
}

export default RootMobileComponent;