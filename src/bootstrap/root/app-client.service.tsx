import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {getPlatformType, PlatformType} from "../../utils/user-agent";

export class AppClient {
  run() {
    var load = this.loadComponentByPlatorm(getPlatformType());
    load((values: { default: React.ComponentClass; })=>{

      ReactDOM.render(
        <values.default/>,
        document.getElementById("root")
      );
    })
  }

  loadComponentByPlatorm(platform:PlatformType) {
    switch (platform) {
      case PlatformType.MOBILE:
        return require('bundle-loader?name=[root.m]!./../root/root-mobile.component');
      case PlatformType.TABLET:
        return require('bundle-loader?name=[root.t]!./../root/root-tablet.component');
      case PlatformType.DESKTOP:
      default:
        return require('bundle-loader?name=[root.d]!./../root/root-desktop.component');
    }
  }
}