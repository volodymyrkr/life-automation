import * as React from "react";
import DocumentHtmlComponent from "common/browser-state/document-html.component";
import SessionModalsComponent from "common/session-modals/session-modals.component";

class RootTabletComponent extends React.Component {
    render() {
        return (
            <div>
                <DocumentHtmlComponent>
                    TABLET
                </DocumentHtmlComponent>
                <SessionModalsComponent/>
            </div>
        )
    }
}

export default RootTabletComponent;