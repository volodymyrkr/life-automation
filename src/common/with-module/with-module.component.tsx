import * as React from "react";
import {Container, interfaces} from "inversify";
import {Provider} from "inversify-react";
import {ComponentClass, ComponentType} from "react";

export interface IModuleConfig {
    addBindings(container: interfaces.Container): void;
}

export interface ConnectedComponent<PropsT> {
    <ComponentT extends ComponentType<PropsT>>(component: ComponentT): ComponentT;
}

export function withModule<ComponentPropsT>(
  ...moduleConfigs: Array<IModuleConfig>
) {
    return function(Component: ComponentClass<ComponentPropsT>) {
        return class extends React.Component<ComponentPropsT, {}> {
            private readonly _displayName = `WithModuleWrapped{${Component.displayName || 'Component'}}`;
            private readonly _container = new Container();

            componentWillMount(): void {
                console.log(this._displayName);
                moduleConfigs.forEach((config:IModuleConfig) => {
                  config.addBindings(this._container);
                })
            }

            render() {
                return (
                    <Provider container={this._container}>
                        <Component {...this.props}/>
                    </Provider>
                )
            }
        }
    } as ConnectedComponent<ComponentPropsT>;
}