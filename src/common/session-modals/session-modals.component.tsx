import * as React from "react";

import "reflect-metadata";
import {resolve} from "inversify-react";
import {observer} from "mobx-react";

import {SessionStore} from "./session.store";

@observer
class SessionModalsComponent extends React.Component {
    @resolve("SessionStore") sessionStore;
    static displayName ='SessionModalsComponent';

      render() {
        return (
            <div className="session-modals" onClick={() => {this.sessionStore.updateSessionId("IIIIIIII")}}>
                SESSION MODALS {this.sessionStore.sessionId}
            </div>
        )
    }
}

export default SessionModalsComponent;