import {injectable} from "inversify";
import {observable, action} from "mobx";

@injectable()
export class SessionStore {
    @observable sessionId="sessionunsession";
    @action updateSessionId = (value) => {
        this.sessionId = "UPDATED TO "+value;
    }
}