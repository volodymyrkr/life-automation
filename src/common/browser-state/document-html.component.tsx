import * as React from "react";

class DocumentHtmlComponent extends React.Component {
    render() {
        return (<div>
            {this.props.children}
        </div>);
    }
}

export default DocumentHtmlComponent;